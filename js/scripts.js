$(function(){
   
    var $window = $(window);
    var $doc = $(document);
    var $items = $('.content');
    var $item = $('.itemScroll');	
    var win_h = window.innerHeight;
    var win_w = window.innerWidth;
    var win_bottom = $doc.scrollTop() + win_h;
    var scroll_run = true;
    var $html_body = $('html, body');
    var $menu = $('.menu');
    var $content = $('.content');
    var $s5_Icon = $('.section5__Icon');
    var $s5_WrapText = $('#s5_wrap_text');
    var $s5_ItemText = $('.section5__itemText');   
    
    
    $('.preload__wrap').addClass('preload_hide');
   
    function is_touch_device() {
        return 'ontouchstart' in window        // works on most browsers 
            || navigator.maxTouchPoints;       // works on IE10/11 and Surface
     };
     
     if(is_touch_device()){
         $('body').addClass('device_touch');
     };
    /*
    $window.on('mousewheel DOMMouseScroll', function(event){
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            activeItem(-200);
        }
        else {
            activeItem(200);
        }
    });
    
    */
   /* 
    $menu.on('mousewheel DOMMouseScroll', function(event){
          event.stopPropagation();
    });
    */
    /*  
     
            var startX,
            startY,
            dist,
            threshold = 80, 
            allowedTime = 800,
            elapsedTime,
            startTime;
            

                $window.on('touchstart', function(event){
                    var touchobj = event.originalEvent.changedTouches[0];
                    dist = 0;
                    startX = touchobj.pageX;
                    startY = touchobj.pageY;
                    startTime = new Date().getTime();
                });
                

            
          
                $window.on('touchend', function(event){
                    var touchobj = event.originalEvent.changedTouches[0];
                    dist = touchobj.pageY - startY;
                    elapsedTime = new Date().getTime() - startTime;
                    if(elapsedTime <= allowedTime && dist >= threshold && Math.abs(touchobj.pageX - startX) <= 300){
                        activeItem(-200);
                    }else if(elapsedTime <= allowedTime && dist <= (-1)*threshold && Math.abs(touchobj.pageX - startX) <= 300){
                        activeItem(200);
                    }
                });
         */
    
    $window.on('scroll', function(e){
        initItem();        
    });
 

    $window.on('resize', function(e){
        win_h = window.innerHeight;
        win_w = window.innerWidth;
        win_bottom = $doc.scrollTop() + win_h;
        section5HeightWrapText();        
    });
      
    $('.menu__panel').on('click', function(){
        $menu.addClass('menu_open');
    });
    
    $menu.on('click', function(){
        $menu.removeClass('menu_open');
    });
    
        
    var $section1Contact = $('.section1__contact');
    
    $('.section1__title_contactButton').on('click',function(){
        $section1Contact.addClass('section1__contact_active');
    });
    
    $('.section1__contactClose').on('click',function(){
        $section1Contact.removeClass('section1__contact_active');
    });

    initItem();
    
    $('.start_open').toggleClass('dropdown_open').children(".dropdown-menu").animate({height: "toggle"}, 400);
    
    $('.dropdown').on('click', function(e){       
        e.preventDefault();
        $(this).toggleClass('dropdown_open').children(".dropdown-menu").animate({height: "toggle"}, 400);
        e.stopPropagation();
    })
    
    
    var $section1Slider = $('.section1__slider');
   
    $section1Slider.owlCarousel({
        items:1,
        dots:false,
        loop:true,
        navSpeed:750,
        animateOut: 'fadeOut',
        autoplay:false,
        autoplayTimeout:5000

    });

    var $section2Slider = $('.section2__slider');

 
    $section2Slider.owlCarousel({
        rtl:true,
        dots:false,
        loop:false,
        nav:true,
        navContainer:'#section2arrows',
        margin:16,
        navSpeed:1000,
        responsiveClass:true,
        responsive:{
            0:{
                items:2,
            },
            1024:{
                margin:32,
                items:2
            },
            1440:{
                items:4,
                margin:30,

            }
        }
    });
    
    var $contentPopup = $('.section2__slideImg');
    
    $popup = $('.popup__overlay');
    $popupItem = $('.popup__item');
    
    $('.popup__close').on('click',function(){
       $popupItem.empty();
       $popup.fadeOut();
    });
    
    $contentPopup.on('click',function(){
       $(this).clone().appendTo($popupItem);
       $popup.fadeTo(500, 1);
    });
    
    var $toggle = $('.section3__toggle');
    var $toggleImg =$('.section3__toggleImg');
    $toggle.on('click',function(){
        $toggle.toggleClass('active');
        $toggleImg.toggleClass('active');
    });

    var $s5_Icon = $('.section5__Icon');
    var $s5_WrapText = $('#s5_wrap_text');
    var $s5_ItemText = $('.section5__itemText');
    
    function section5Start(){
        
       if($s5_Icon.hasClass('active')){
           var s5_textID = $s5_Icon.filter('.active').attr("data-text-id");

           $s5_ItemText.not(s5_textID).removeClass('active');
           $(s5_textID).addClass('active');
       } else {
           $s5_Icon.first().addClass('active').attr("data-text-id");
           var s5_textID = $s5_Icon.first().addClass('active').attr("data-text-id");
           $s5_ItemText.not(s5_textID).removeClass('active');
           $(s5_textID).addClass('active');
       }
    };
    
    function section5HeightWrapText(){
        var maxH = 0;
        $s5_ItemText.each(function(){
           var h = $(this).outerHeight();
           if(h > maxH){
               maxH = h;
           }
        });
        $s5_WrapText.css('padding-top', maxH + 'px');
    };
    
    
    section5Start();
    
    section5HeightWrapText();
    
    $s5_Icon.on('click',function(){
        if(!($(this).hasClass('active'))){
        $s5_Icon.removeClass('active');
        var s5_textID = $(this).addClass('active').attr("data-text-id");
        $s5_ItemText.not(s5_textID).removeClass('active');
        $(s5_textID).addClass('active');
        }
    });
    
    function scrollEnd(){
        setTimeout(function() {  
            scroll_run = true;
        }, 50);                   
    };
    
    var $section6Slider = $('.section6__slider');
   
    $section6Slider.owlCarousel({
        items:1,
        startPosition:1,
        dots:true,
        loop:true,
        nav:true,
        navContainer:'#section6arrows',
        dotsContainer:'#section6dots',
        navSpeed:750,
        //animateOut: 'fadeOut',
        autoplay:false

    });
    
    function scrollAnimate(top){
        scroll_run = false;
        var padding = 0;
        if(win_w < 1024){
            padding -= 79;
        };
        
        var sTop = top + padding;
        $html_body.animate({
            scrollTop: sTop
        }, 700, $.bez([.25,.1,.25,1]), scrollEnd);
    };

    function initItem(){
        var docTop = $doc.scrollTop();        
        var win_center = docTop + win_h/2;      
        var targetTop, $targetIn, target = false, target2 = false;
        
        if(docTop < win_h/2){
            $menu.addClass('menu_active');
        } else {
            $menu.removeClass('menu_active');
        }

        $item.each(function () {

            if(!target){
                $this = $(this); 
                var itemTop = $this.offset().top;
                var itemBot = itemTop + win_h;
                
                if(itemTop > (docTop - 1) && itemTop < win_center){
                    activeSection($this);
                    target = true;
                }
                if(itemTop < docTop){
                    if(!target2){
                            targetTop = itemTop;
                            $targetIn = $this;
                            target2 = true;
                    }else if(itemTop > targetTop){
                        targetTop = itemTop;
                        $targetIn = $this;
                    }                            
                }
            }    
        });
        if(!target){
             activeSection($targetIn);
        }
        
        function activeSection($active){
            $items.removeClass('section1-active section2-active section3-active section4-active section5-active section6-active section7-active').addClass($active.attr("data-scontrol"));
        }
    };

    function activeItem(step){


        var step = parseInt(step, 10);

        if(scroll_run){

            var docTop = $doc.scrollTop();
            
            var paddingTop = 0;
                if(win_w < 1024){
                    paddingTop += 79;
                };

            docTop += paddingTop;
            
            win_bottom = docTop + win_h;
            var targetTop, target, $target, targetBottom, targetTopUp, target2 = false; 


            if(step > 0){
                target = false;
                $item.each(function () {
                    $this = $(this); 
                    var itemTop = $this.offset().top;


                    if(itemTop > docTop + 10 && itemTop < win_bottom + 10){
                        if(!target){
                                targetTop = itemTop;
                                target = true;
                                $target = $this;
                        }else{
                                if(itemTop < targetTop){
                                        targetTop = itemTop;
                                        $target = $this;
                                }
                        }
                    }
                    if(!target){
                        var itemBot = itemTop + $this.outerHeight();
                        if(itemBot > win_bottom && itemBot < win_bottom + win_h){
                            if(!target2){
                                targetBottom = itemBot;
                                target2 = true;
                            }else{
                                if(targetBottom > itemBot){
                                    targetBottom = itemBot;
                                }
                            }
                        }
                    }
                });
                if(target){
                    scrollAnimate(targetTop);              
                }else{                        
                    if(target2){                           
                        scrollAnimate(targetBottom - win_h);
                    }
                    else{
                        scrollAnimate(docTop + (win_h - 10));
                    }
                }
            }else if(step < 0){
                target = false;			
                $item.each(function () {
                    $this = $(this); 
                    var itemTop = $this.offset().top;
                    var itemBot = itemTop + $this.outerHeight();
                    var targetBot;
                    if(itemBot > docTop - 10 && itemBot < win_bottom){

                        if(!target){
                            targetTop = itemTop;
                            targetBot = itemBot;
                            target = true;
                            $target = $this;
                        }else{
                            if(itemBot < targetBot){
                                    targetTop = itemTop;
                                    $target = $this;
                            }
                        }
                    }
                    if(!target){

                        if(itemTop > docTop - win_h && itemTop < docTop){
                            if(!target2){
                                targetTopUp = itemTop;
                                target2 = true;
                            }else{
                                if(targetTopUp < itemTop){
                                    targetTopUp = itemTop;
                                }
                            }
                        }
                    }
                });
                if(target){
                    scrollAnimate(targetTop);
                }else{
                    if(target2){
                        scrollAnimate(targetTopUp);
                    }else{
                        scrollAnimate(docTop - (win_h - 10));
                    }
                }
            }
        }
    }
    
    $('.section6__slideImg_zoom').each(function () {

        $(this).okzoom({
            width: 100,
            height: 100,
            shadow: "0 0 2vw rgba(0,0,0,0.5)"
          });
        });
    


});
