

ymaps.ready(function () {
    var mapK = new ymaps.Map('map-k', {
            center: [60.085731, 30.512691],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        }),


        myPlacemarkO = new ymaps.Placemark([59.979227, 30.324550], {
            hintContent: 'Офис',
            balloonContent: ''
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: './img/icon-o.png',
            // Размеры метки.
            iconImageSize: [78, 99],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-39, -99]
        }),

        myPlacemarkK = new ymaps.Placemark([60.193259, 30.357509], {
            hintContent: 'Комплеск',
            balloonContent: ''
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: './img/icon-k.png',
            // Размеры метки.
            iconImageSize: [78, 99],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-39, -99]
        });

    mapK.geoObjects
        .add(myPlacemarkO)
        .add(myPlacemarkK);

    var mapF = new ymaps.Map('map-7s', {
            center: [60.085731, 30.512691],
            zoom: 9
        }, {
            searchControlProvider: 'yandex#search'
        }),


        myPlacemarkOf = new ymaps.Placemark([59.979227, 30.324550], {
            hintContent: 'Офис',
            balloonContent: ''
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: './img/icon-o.png',
            // Размеры метки.
            iconImageSize: [78, 99],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-39, -99]
        }),

        myPlacemarkKf = new ymaps.Placemark([60.193259, 30.357509], {
            hintContent: 'Комплеск',
            balloonContent: ''
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: './img/icon-k.png',
            // Размеры метки.
            iconImageSize: [78, 99],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-39, -99]
        });

    mapF.geoObjects
        .add(myPlacemarkOf)
        .add(myPlacemarkKf);
});